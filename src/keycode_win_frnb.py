# SPDX-FileCopyrightText: 2022 Neradoc NeraOnGit@ri1.fr
# SPDX-License-Identifier: MIT
"""
This file was automatically generated using Circuitpython_Keyboard_Layouts
"""


__version__ = "0.0.0-auto.0"
__repo__ = "https://github.com/Neradoc/Circuitpython_Keyboard_Layouts.git"


class Keycode:
    A = 0x04
    B = 0x14
    C = 0x0b
    D = 0x0c
    E = 0x09
    F = 0x38
    G = 0x36
    H = 0x37
    I = 0x07
    J = 0x13
    K = 0x05
    L = 0x12
    M = 0x34
    N = 0x33
    O = 0x15
    P = 0x08
    Q = 0x10
    R = 0x0f
    S = 0x0e
    T = 0x0d
    U = 0x16
    V = 0x18
    W = 0x30
    X = 0x06
    Y = 0x1b
    Z = 0x2f
    ALT = 0xe2
    END = 0x4d
    F1 = 0x3a
    F2 = 0x3b
    F3 = 0x3c
    F4 = 0x3d
    F5 = 0x3e
    F6 = 0x3f
    F7 = 0x40
    F8 = 0x41
    F9 = 0x42
    F10 = 0x43
    F11 = 0x44
    F12 = 0x45
    F13 = 0x68
    F14 = 0x69
    F15 = 0x6a
    F16 = 0x6b
    F17 = 0x6c
    F18 = 0x6d
    F19 = 0x6e
    F20 = 0x6f
    F21 = 0x70
    F22 = 0x71
    F23 = 0x72
    F24 = 0x73
    GUI = 0xe3
    ONE = 0x1e
    SIX = 0x23
    TAB = 0x2b
    TWO = 0x1f
    FIVE = 0x22
    FOUR = 0x21
    HOME = 0x4a
    NINE = 0x26
    ZERO = 0x27
    ALTGR = 0xe6
    Brève = 0x0f
    COMMA = 0x0a
    Caron = 0x18
    EIGHT = 0x25
    ENTER = 0x28
    MINUS = 0x2d
    PAUSE = 0x48
    QUOTE = 0x35
    SEVEN = 0x24
    SHIFT = 0xe1
    SPACE = 0x2c
    THREE = 0x20
    Tilde = 0x33
    Tréma = 0x07
    APPLICATION = 0x65
    Accent_aigu = 0x1a
    Accent_circonflexe = 0x1c
    Accent_grave = 0x17
    BACKSLASH = 0x31
    BACKSPACE = 0x2a
    Barre_couvrante = 0x2f
    CAPS_LOCK = 0x39
    COMMAND = 0xe3
    CONTROL = 0xe0
    Cédille = 0x0b
    DELETE = 0x4c
    DOWN_ARROW = 0x51
    EQUALS = 0x2e
    ESCAPE = 0x29
    Exposants = 0x0d
    FORWARD_SLASH = 0x17
    GRAVE_ACCENT = 0x11
    INSERT = 0x49
    KEYPAD_ASTERISK = 0x55
    KEYPAD_EIGHT = 0x60
    KEYPAD_FIVE = 0x5d
    KEYPAD_FORWARD_SLASH = 0x54
    KEYPAD_FOUR = 0x5c
    KEYPAD_MINUS = 0x56
    KEYPAD_NINE = 0x61
    KEYPAD_NUMLOCK = 0x53
    KEYPAD_ONE = 0x59
    KEYPAD_PERIOD = 0x63
    KEYPAD_PLUS = 0x57
    KEYPAD_SEVEN = 0x5f
    KEYPAD_SIX = 0x5e
    KEYPAD_THREE = 0x5b
    KEYPAD_TWO = 0x5a
    KEYPAD_ZERO = 0x62
    LEFT_ALT = 0xe2
    LEFT_ARROW = 0x50
    LEFT_BRACKET = 0x1c
    LEFT_CONTROL = 0xe0
    LEFT_GUI = 0xe3
    LEFT_SHIFT = 0xe1
    Latin_et_ponctuation = 0x0e
    Lettres_grecques = 0x36
    Macron = 0x34
    OEM_102 = 0x64
    OPTION = 0xe2
    Ogonek = 0x38
    PAGE_DOWN = 0x4e
    PAGE_UP = 0x4b
    PERIOD = 0x19
    PRINT_SCREEN = 0x46
    Point_souscrit = 0x37
    RETURN = 0x28
    RIGHT_ALT = 0xe6
    RIGHT_ARROW = 0x4f
    RIGHT_BRACKET = 0x1d
    RIGHT_CONTROL = 0xe4
    RIGHT_GUI = 0xe7
    RIGHT_SHIFT = 0xe5
    Rond_en_chef = 0x10
    SCROLL_LOCK = 0x47
    SEMICOLON = 0x1a
    SPACEBAR = 0x2c
    UP_ARROW = 0x52
    WINDOWS = 0xe3
    Symboles_scientifiques = 0x0c
    Barre_oblique_couvrante = 0x12

    @classmethod
    def modifier_bit(cls, keycode):
        """Return the modifer bit to be set in an HID keycode report if this is a
        modifier key; otherwise return 0."""
        return (
            1 << (keycode - 0xE0) if cls.LEFT_CONTROL <= keycode <= cls.RIGHT_GUI else 0
        )
    