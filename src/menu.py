import skeleton

def build_application(apps):

    enabled_apps = [app for app in apps if app.visible]
    max_l = min(12, len(enabled_apps))
    configuration = skeleton.Configuration("Application menu")
    for index in range(max_l):
        if enabled_apps[index] is None:
            break

        # I use a lambda function here, the index is stored in a closure
        # otherwise the value is erased and only keep the last iteration
        # index
        callback = lambda n:lambda macropad, key, pressed:enabled_apps[n]

        configuration.registerKey(
                index,
                enabled_apps[index].title,
                callback(index),
                None
            )
    return configuration
