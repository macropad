.. -*- mode: rst -*-
.. -*-  coding: utf-8 -*-

Application macropad

Utilisation
===========

Activer le système de fichier
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Lors du démarrage, le système de fichier n’est pas monté. C’est à dire que
le clavier n’apparait pas comme un lecteur auprès du système.

Pour activer cette fonctionnalité, il faut appuyer sur la touche située en bas
à droite du clavier pendant le démarrage du clavier.

::

    . . .
    . . .
    . . .
    . . X

Ce code est géré dans le fichier "boot.py"

Pilotage par port série
~~~~~~~~~~~~~~~~~~~~~~~

L’application peut recevoir des commandes sur le port série : l’envoi d’un json
tel que :

.. code:: json

    [{"layout": "valeur à charger"}]

va changer le programme actif sur le macropad pour celui qui a été donné en
paramètre.

De la meme manière, le programme envoie sur le port-série les changements qui
sont provoqués par l’utilisateur. Ainsi, il est possible d’avoir une
application sur le PC qui se synchronise avec le Macropad sur la disposition à
charger.


Code
====

Configuration clavier
~~~~~~~~~~~~~~~~~~~~~

Le clavier ne connait pas les codes correspondants aux touches saisies.
L’application n’est capable que d’emettre les codes des touches, mais cela
n’aide pas la construction d’une application lisible.

Les codes BÉPO sont disponibles dans le fichier `keycode_win_frnb.py` qui fait
l’association des différentes touches :

.. code:: python

    from keycode_win_frnb import Keycode # Use the french bepo layout

    def reload(macropad, key, pressed):
        Action().key(pressed, [Keycode.CONTROL, Keycode.R])

Compilation
===========

Les fichiers mpy sont des fichiers compilés correspondants aux fichier py mais
occupants moins de place sur le disque. Ils doivent etre compilés à l’aide de
l’outil mpy-cross qui est téléchargeable ici :

https://adafruit-circuit-python.s3.amazonaws.com/index.html?prefix=bin/mpy-cross/
