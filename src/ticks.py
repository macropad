from supervisor import ticks_ms

# See the example at
# https://docs.circuitpython.org/en/latest/shared-bindings/supervisor/index.html?#supervisor.ticks_ms
_TICKS_PERIOD = const(1<<29)
_TICKS_MAX = const(_TICKS_PERIOD-1)
_TICKS_HALFPERIOD = const(_TICKS_PERIOD//2)

def add(ticks, delta):
    "Add a delta to a base number of ticks, performing wraparound at 2**29ms."
    return (ticks + delta) % _TICKS_PERIOD

def diff(ticks1, ticks2):
    "Compute the signed difference between two ticks values, assuming that they are within 2**28 ticks"
    diff = (ticks1 - ticks2) & _TICKS_MAX
    diff = ((diff + _TICKS_HALFPERIOD) & _TICKS_MAX) - _TICKS_HALFPERIOD
    return diff

def less(ticks):
    "Return true iff ticks1 is less than ticks2, assuming that they are within 2**28 ticks"
    return diff(ticks_ms(), ticks) < 0
