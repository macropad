# This example is for the MacroPad,
# or any board with buttons that are connected to ground when pressed.

import usb_cdc
import storage
import board, digitalio

# On the Macropad, pressing a key grounds it. You need to set a pull-up.
# If not pressed, the key will be at +V (due to the pull-up).
# The KEY12 is the RightBotom key in the keypad
button = digitalio.DigitalInOut(board.KEY12)
button.pull = digitalio.Pull.UP

usb_cdc.enable(console=True, data=True)
# Disable devices only if button is not pressed.
if button.value:
	print("KEY12 Unpressed, starting without storage")
	storage.disable_usb_drive()
else:
	print("Mounting USB storage")
