#!/usr/bin/env python
# -*- coding: utf-8 -*-

# The firmware I’m using does not have the function display_sleep available. So
# I’m sending the raw code directly.
#
# https://docs.circuitpython.org/projects/macropad/en/latest/api.html#adafruit_macropad.MacroPad.display_sleep
#

timeout = 15 * 60 * 1000
from supervisor import ticks_ms
import ticks

class Power(object):

    def __init__(self, macropad):
        self.macropad = macropad
        self.last_update = ticks_ms()
        self.configuration = None
        self.sleep = False
        self.macropad.display.brightness = 0
        self.macropad.display.bus.send(0xAF, b"")

    def start(self):
        self.last_update = ticks_ms()
        self._wake()

    def set_configuration(self, configuration):
        self.configuration = configuration
        self.start()

    def _sleep(self):
        if self.sleep:
            return
        self.sleep = True
        self.macropad.pixels.fill((0, 0, 0))
        self.macropad.pixels.show()
        self.macropad.display.bus.send(0xAE, b"")

    def _wake(self):
        if not self.sleep:
            return
        self.sleep = False
        #self.macropad.display_sleep = False
        for index, value in self.configuration.keys.items():
            if value is None or index >= 12:
                continue
            self.macropad.pixels[index] = value.color
        self.macropad.pixels.show()
        self.macropad.display.bus.send(0xAF, b"")

    def tick(self):
        if ticks.diff(ticks_ms(), self.last_update) > timeout:
            self._sleep()

    def execute_action(self, key, pressed):
        self.start()
