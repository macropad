# SPDX-FileCopyrightText: 2021 Phillip Burgess for Adafruit Industries
#
# SPDX-License-Identifier: MIT

# MACROPAD Hotkeys example: Mouse control

# The syntax for Mouse macros is highly peculiar, in order to maintain
# backward compatibility with the original keycode-only macro files.
# The third item for each macro is a list in brackets, and each value within
# is normally an integer (Keycode), float (delay) or string (typed literally).
# Consumer Control codes were added as list-within-list, and then mouse
# further complicates this by adding dicts-within-list. Each mouse-related
# dict can have any mix of keys 'buttons' w/integer mask of button values
# (positive to press, negative to release), 'x' w/horizontal motion,
# 'y' w/vertical and 'wheel' with scrollwheel motion.

# To reference Mouse constants, import Mouse like so...
from keycode_win_frnb import Keycode # Use the french bepo layout

import skeleton
from actions import Action

def wc(macropad, key, pressed):
    if not pressed:
        return
    Action().sequence(
            [ [Keycode.KEYPAD_FORWARD_SLASH]
            , [Keycode.W]
            , [Keycode.C]
            , [Keycode.ENTER]
            ]
        )

def key(code):
    def action(macropad, key, pressed):
        Action().key(pressed, code)
    return action


def build_application():

    configuration = skeleton.Configuration("Irssi")
    configuration.visible = True
    configuration.registerKey(0,  "next.",      key( [Keycode.ALT, Keycode.A]),       0x000200)
    configuration.registerKey(2,  "/wc",        wc,        0x020000)
    configuration.registerKey(3,  "<",          key([Keycode.CONTROL, Keycode.P]), 0x050505)
    configuration.registerKey(5,  ">",          key([Keycode.CONTROL, Keycode.N]),   0x050505)


    return configuration

configuration = build_application()


