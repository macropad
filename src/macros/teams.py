# SPDX-FileCopyrightText: 2021 Phillip Burgess for Adafruit Industries
#
# SPDX-License-Identifier: MIT

# MACROPAD Hotkeys example: Mouse control

# The syntax for Mouse macros is highly peculiar, in order to maintain
# backward compatibility with the original keycode-only macro files.
# The third item for each macro is a list in brackets, and each value within
# is normally an integer (Keycode), float (delay) or string (typed literally).
# Consumer Control codes were added as list-within-list, and then mouse
# further complicates this by adding dicts-within-list. Each mouse-related
# dict can have any mix of keys 'buttons' w/integer mask of button values
# (positive to press, negative to release), 'x' w/horizontal motion,
# 'y' w/vertical and 'wheel' with scrollwheel motion.

# To reference Mouse constants, import Mouse like so...
from keycode_win_frnb import Keycode # Use the french bepo layout

import skeleton
from actions import Action

def conv(macropad, key, pressed):
    Action().key(pressed, [Keycode.CONTROL, Keycode.TWO])

def planning(macropad, key, pressed):
    Action().key(pressed, [Keycode.CONTROL, Keycode.FOUR])

def link(macropad, key, pressed):
    Action().key(pressed, [Keycode.CONTROL, Keycode.K])

def mic(macropad, key, pressed):
    Action().key(pressed, [Keycode.SHIFT, Keycode.CONTROL, Keycode.M])

def camera(macropad, key, pressed):
    Action().key(pressed, [Keycode.SHIFT, Keycode.CONTROL, Keycode.O])

def build_application():

    configuration = skeleton.Configuration("Teams")
    configuration.visible = False
    configuration.registerKey(0,  "Convers.",   conv,       0x050000)
    configuration.registerKey(3,  "Planning",   planning,   0x050200)


    configuration.registerKey(6,  "Micro.", mic,        0x000202)
    configuration.registerKey(9,  "camera", camera,         0x020200)

    configuration.registerKey(2,  "Link",       link,       0x020002)

    return configuration

configuration = build_application()
