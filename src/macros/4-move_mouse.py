import skeleton

class Color(object):
    """ Return a color which may change over time.
    """
    def __init__(self, code):
        self.code = code

    def __int__(self):
        return self.code

class NoIdle(object):

    def __init__(self):
        self.x = 10
        self.color = Color(0x000500)
        self.active = False

    def update(self, macropad):
        if not self.active:
            return

        self.x *= -1
        macropad.mouse.move(
                 self.x,
                 0,
                 0)
        # Update the color in red or blue in order to show that the heartbeat
        # is active.
        self.color.code = 0x010000 if self.x > 0 else 0x000001
        macropad.pixels[4] = int(self.color)
        macropad.pixels.show()

    def pause(self, macropad, key, pressed):
        if pressed:
            self.active = not self.active
        if self.active:
            self.update(macropad)
        else:
            # Restore the default color (green)
            self.color.code = 0x000500
            macropad.pixels[4] = int(self.color)
            macropad.pixels.show()


def build_application():
    configuration = skeleton.Configuration("NoIdle")
    actor = NoIdle()
    configuration.setTick(5000, actor.update)
    configuration.registerKey(4,  "switch",   actor.pause,    actor.color)
    configuration.is_layout = False
    return configuration

configuration = build_application()

