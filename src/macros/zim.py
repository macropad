# SPDX-FileCopyrightText: 2021 Phillip Burgess for Adafruit Industries
#
# SPDX-License-Identifier: MIT

# MACROPAD Hotkeys example: Mouse control

# The syntax for Mouse macros is highly peculiar, in order to maintain
# backward compatibility with the original keycode-only macro files.
# The third item for each macro is a list in brackets, and each value within
# is normally an integer (Keycode), float (delay) or string (typed literally).
# Consumer Control codes were added as list-within-list, and then mouse
# further complicates this by adding dicts-within-list. Each mouse-related
# dict can have any mix of keys 'buttons' w/integer mask of button values
# (positive to press, negative to release), 'x' w/horizontal motion,
# 'y' w/vertical and 'wheel' with scrollwheel motion.

# To reference Mouse constants, import Mouse like so...
from keycode_win_frnb import Keycode # Use the french bepo layout

import skeleton
from actions import Action

def t1(macropad, key, pressed):
    Action().key(pressed, [Keycode.CONTROL, Keycode.SHIFT, Keycode.ONE])

def t2(macropad, key, pressed):
    Action().key(pressed, [Keycode.CONTROL, Keycode.SHIFT, Keycode.TWO])

def t3(macropad, key, pressed):
    Action().key(pressed, [Keycode.CONTROL, Keycode.SHIFT, Keycode.THREE])

def none(macropad, key, pressed):
    Action().key(pressed, [Keycode.CONTROL, Keycode.SHIFT, Keycode.NINE])

def link(macropad, key, pressed):
    Action().key(pressed, [Keycode.CONTROL, Keycode.L])

def build_application():

    configuration = skeleton.Configuration("Zim")
    configuration.visible = False
    configuration.registerKey(0,  "T1",     t1,       0x000500)
    configuration.registerKey(3,  "T2",     t2,       0x000200)
    configuration.registerKey(6,  "T3",     t3,       0x000100)
    configuration.registerKey(9,  "None",   none,     0x000000)

    configuration.registerKey(2,  "Link",   link,     0x020002)


    return configuration

configuration = build_application()
