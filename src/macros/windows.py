from keycode_win_frnb import Keycode # Use the french bepo layout

import skeleton
from actions import Action

def tab(macropad, key, pressed):
    Action().key(pressed, [Keycode.RIGHT_ALT, Keycode.TAB])

def win_l(macropad, key, pressed):
    Action().key(pressed, [Keycode.CONTROL, Keycode.WINDOWS,  Keycode.LEFT_ARROW])

def win_r(macropad, key, pressed):
    Action().key(pressed, [Keycode.CONTROL, Keycode.WINDOWS,  Keycode.RIGHT_ARROW])

def close(macropad, key, pressed):
    Action().key(pressed, [Keycode.ALT, Keycode.F4])

def rot_cw(macropad, key, pressed):
    Action().key(pressed, [Keycode.RIGHT_ARROW])

def rot_ccw(macropad, key, pressed):
    Action().key(pressed, [Keycode.LEFT_ARROW])

def build_application():
    configuration = skeleton.Configuration("Windows")
    configuration.hidden = True

    configuration.registerKey(2,   "x",  close,         0x050000)
    configuration.registerKey(7,   "^<", win_l,         0x000000)
    configuration.registerKey(8,   "^>", win_r,         0x000000)
    configuration.registerKey(11,  "^TAB", tab,         0x000000)
    configuration.registerKey(12,  "",   rot_cw,        None)
    configuration.registerKey(13,  "",   rot_ccw,       None)
    return configuration

configuration = build_application()
