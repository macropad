# SPDX-FileCopyrightText: 2021 Phillip Burgess for Adafruit Industries
#
# SPDX-License-Identifier: MIT

# MACROPAD Hotkeys example: Mouse control

# The syntax for Mouse macros is highly peculiar, in order to maintain
# backward compatibility with the original keycode-only macro files.
# The third item for each macro is a list in brackets, and each value within
# is normally an integer (Keycode), float (delay) or string (typed literally).
# Consumer Control codes were added as list-within-list, and then mouse
# further complicates this by adding dicts-within-list. Each mouse-related
# dict can have any mix of keys 'buttons' w/integer mask of button values
# (positive to press, negative to release), 'x' w/horizontal motion,
# 'y' w/vertical and 'wheel' with scrollwheel motion.

# To reference Mouse constants, import Mouse like so...
from adafruit_hid.keycode import Keycode # REQUIRED if using Keycode.* values
from adafruit_hid.mouse import Mouse

import skeleton
from actions import Action

def desktop4(macropad, key, pressed):
    Action().key(pressed, [Keycode.RIGHT_ALT, Keycode.F4])

def desktop3(macropad, key, pressed):
    Action().key(pressed, [Keycode.RIGHT_ALT, Keycode.F3])

def desktop2(macropad, key, pressed):
    Action().key(pressed, [Keycode.RIGHT_ALT, Keycode.F2])

def desktop1(macropad, key, pressed):
    Action().key(pressed, [Keycode.RIGHT_ALT, Keycode.F1])

def rot_cw(macropad, key, pressed):
    Action().key(pressed, [Keycode.GUI])
    if pressed:
        macropad.mouse.move(
                 0,
                 0,
                -1)

def rot_acw(macropad, key, pressed):
    Action().key(pressed, [Keycode.GUI])
    if pressed:
        macropad.mouse.move(
                 0,
                 0,
                 1)

def build_application():
    configuration = skeleton.Configuration("WM")
    configuration.registerKey(2,  "F4", desktop4, None)
    configuration.registerKey(5,  "F3", desktop3, None)
    configuration.registerKey(8,  "F2", desktop2, None)
    configuration.registerKey(11, "F1", desktop1, None)
    configuration.registerKey(12, "",   rot_cw,   None)
    configuration.registerKey(13, "",   rot_acw,  None)
    return configuration

configuration = build_application()
