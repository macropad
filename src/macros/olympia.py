# SPDX-FileCopyrightText: 2021 Phillip Burgess for Adafruit Industries
#
# SPDX-License-Identifier: MIT

# MACROPAD Hotkeys example: Mouse control

# The syntax for Mouse macros is highly peculiar, in order to maintain
# backward compatibility with the original keycode-only macro files.
# The third item for each macro is a list in brackets, and each value within
# is normally an integer (Keycode), float (delay) or string (typed literally).
# Consumer Control codes were added as list-within-list, and then mouse
# further complicates this by adding dicts-within-list. Each mouse-related
# dict can have any mix of keys 'buttons' w/integer mask of button values
# (positive to press, negative to release), 'x' w/horizontal motion,
# 'y' w/vertical and 'wheel' with scrollwheel motion.

# To reference Mouse constants, import Mouse like so...
from keycode_win_frnb import Keycode # Use the french bepo layout
import colorsys

import skeleton
from actions import Action


class Color(object):
    """ Represent the color of a key in the HSV model.

    The class implements the int() function in order to being compatible with
    the remaining of the application.
    """
    def __init__(self, h, s, v):
        self.col = (h, s, v)

    def __int__(self):
        r, g, b = colorsys.hsv_to_rgb(self.col[0], self.col[1], self.col[2])
        return (r<<16) + (g<<8) + b

def key(code):
    def action(macropad, key, pressed):
        Action().key(pressed, code)
    return action

def build_application():
    configuration = skeleton.Configuration("Olympia")
    configuration.hidden = False
    configuration.registerKey(0,  "vue",  key([Keycode.V]),        0x000500)
    configuration.registerKey(1,  "map",  key([Keycode.M]),        0x000005)
    configuration.registerKey(2,  "x",    key([Keycode.CONTROL, Keycode.W]),      0x050000)
    configuration.registerKey(3,  "\\",   key([Keycode.KEYPAD_SEVEN]), Color(0.333, 1.0, 0.0196))
    configuration.registerKey(
            4,
            "|",
            lambda macropad, key, pressed: Action().key(pressed, [Keycode.KEYPAD_EIGHT]),
            Color(0.266, 1.0, 0.0196))
    configuration.registerKey(
            5,
            "/",
            lambda macropad, key, pressed: Action().key(pressed, [Keycode.KEYPAD_NINE]),
            Color(0.166, 1.0, 0.0196))
    configuration.registerKey(
            6,
            "<",
            lambda macropad, key, pressed: Action().key(pressed, [Keycode.KEYPAD_FOUR]),
            Color(0.4, 1.0, 0.0196))

    configuration.registerKey(7,  "Inventaire",  key([Keycode.I]), Color(0.333, 0.6, 0.0196))

    configuration.registerKey(
            8,
            ">",
            lambda macropad, key, pressed: Action().key(pressed, [Keycode.KEYPAD_SIX]),
            # 0x050502
            Color(0.16, 0.6, 0.0196))
    configuration.registerKey(
            9,
            "/",
            key([Keycode.KEYPAD_ONE]),
            0x000505)
    configuration.registerKey(
            10,
            "|",
            lambda macropad, key, pressed: Action().key(pressed, [Keycode.KEYPAD_TWO]),
            Color(0.5, 0.6, 0.0196))
    configuration.registerKey(
            11,
            "\\",
            key([Keycode.KEYPAD_THREE]),
            0x050505)
    return configuration

configuration = build_application()
