# SPDX-FileCopyrightText: 2021 Phillip Burgess for Adafruit Industries
#
# SPDX-License-Identifier: MIT

# MACROPAD Hotkeys example: Mouse control

# The syntax for Mouse macros is highly peculiar, in order to maintain
# backward compatibility with the original keycode-only macro files.
# The third item for each macro is a list in brackets, and each value within
# is normally an integer (Keycode), float (delay) or string (typed literally).
# Consumer Control codes were added as list-within-list, and then mouse
# further complicates this by adding dicts-within-list. Each mouse-related
# dict can have any mix of keys 'buttons' w/integer mask of button values
# (positive to press, negative to release), 'x' w/horizontal motion,
# 'y' w/vertical and 'wheel' with scrollwheel motion.

# To reference Mouse constants, import Mouse like so...
from adafruit_hid.keycode import Keycode # REQUIRED if using Keycode.* values

import skeleton
from actions import Action

def build_application():
    configuration = skeleton.Configuration("F-Keys")
    configuration.registerKey(
            0,
            "F13",
            lambda macropad, key, pressed: Action().key(pressed, [Keycode.F13]),
            None)
    configuration.registerKey(
            1,
            "F14",
            lambda macropad, key, pressed: Action().key(pressed, [Keycode.F14]),
            None)
    configuration.registerKey(
            2,
            "F15",
            lambda macropad, key, pressed: Action().key(pressed, [Keycode.F15]),
            None)
    configuration.registerKey(
            3,
            "F16",
            lambda macropad, key, pressed: Action().key(pressed, [Keycode.F16]),
            None)
    configuration.registerKey(
            4,
            "F17",
            lambda macropad, key, pressed: Action().key(pressed, [Keycode.F17]),
            None)
    configuration.registerKey(
            5,
            "F18",
            lambda macropad, key, pressed: Action().key(pressed, [Keycode.F18]),
            None)
    configuration.registerKey(
            6,
            "F19",
            lambda macropad, key, pressed: Action().key(pressed, [Keycode.F19]),
            None)
    configuration.registerKey(
            7,
            "F20",
            lambda macropad, key, pressed: Action().key(pressed, [Keycode.F20]),
            None)
    configuration.registerKey(
            8,
            "F21",
            lambda macropad, key, pressed: Action().key(pressed, [Keycode.F21]),
            None)
    configuration.registerKey(
            9,
            "F22",
            lambda macropad, key, pressed: Action().key(pressed, [Keycode.F22]),
            None)
    configuration.registerKey(
            10,
            "F23",
            lambda macropad, key, pressed: Action().key(pressed, [Keycode.F23]),
            None)
    configuration.registerKey(
            11,
            "F24",
            lambda macropad, key, pressed: Action().key(pressed, [Keycode.F24]),
            None)
    return configuration

configuration = build_application()
