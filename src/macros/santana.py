# SPDX-FileCopyrightText: 2021 Phillip Burgess for Adafruit Industries
#
# SPDX-License-Identifier: MIT

# MACROPAD Hotkeys example: Mouse control

# The syntax for Mouse macros is highly peculiar, in order to maintain
# backward compatibility with the original keycode-only macro files.
# The third item for each macro is a list in brackets, and each value within
# is normally an integer (Keycode), float (delay) or string (typed literally).
# Consumer Control codes were added as list-within-list, and then mouse
# further complicates this by adding dicts-within-list. Each mouse-related
# dict can have any mix of keys 'buttons' w/integer mask of button values
# (positive to press, negative to release), 'x' w/horizontal motion,
# 'y' w/vertical and 'wheel' with scrollwheel motion.

# To reference Mouse constants, import Mouse like so...
from keycode_win_frnb import Keycode # Use the french bepo layout
from adafruit_hid.mouse import Mouse

import skeleton
from actions import Action

def groovy(macropad, key, pressed):
    Action().key(pressed, [Keycode.CONTROL, Keycode.G])

def log(macropad, key, pressed):
    Action().key(pressed, [Keycode.CONTROL, Keycode.L])

def dql(macropad, key, pressed):
    Action().key(pressed, [Keycode.CONTROL, Keycode.SHIFT, Keycode.F])

def package(macropad, key, pressed):
    Action().key(pressed, [Keycode.CONTROL, Keycode.P])

def build_application():

    configuration = skeleton.Configuration("Santana")
    configuration.visible = False
    configuration.registerKey(0,  "groovy", groovy,   0x000500)
    configuration.registerKey(1,  "logs",   log,      0x000005)

    configuration.registerKey(3,  "dql",    dql,      0x050500)
    configuration.registerKey(4,  "package",package,  0x050000)
    return configuration

configuration = build_application()
