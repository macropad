# SPDX-FileCopyrightText: 2021 Phillip Burgess for Adafruit Industries
#
# SPDX-License-Identifier: MIT

# MACROPAD Hotkeys example: Mouse control

# The syntax for Mouse macros is highly peculiar, in order to maintain
# backward compatibility with the original keycode-only macro files.
# The third item for each macro is a list in brackets, and each value within
# is normally an integer (Keycode), float (delay) or string (typed literally).
# Consumer Control codes were added as list-within-list, and then mouse
# further complicates this by adding dicts-within-list. Each mouse-related
# dict can have any mix of keys 'buttons' w/integer mask of button values
# (positive to press, negative to release), 'x' w/horizontal motion,
# 'y' w/vertical and 'wheel' with scrollwheel motion.

# To reference Mouse constants, import Mouse like so...
from keycode_win_frnb import Keycode # Use the french bepo layout
from adafruit_hid.mouse import Mouse

import skeleton
from actions import Action

def reload(macropad, key, pressed):
    Action().key(pressed, [Keycode.CONTROL, Keycode.R])

def tab(macropad, key, pressed):
    Action().key(pressed, [Keycode.CONTROL, Keycode.T])

def close(macropad, key, pressed):
    Action().key(pressed, [Keycode.CONTROL, Keycode.W])

def page_up(macropad, key, pressed):
    macropad.mouse.move(
             0,
             0,
             1)

def page_down(macropad, key, pressed):
    macropad.mouse.move(
             0,
             0,
             -1)

class App(object):
    def __init__(self):
        self.group = skeleton.Group(0x050500,
                [(9,  "Page")
                ,(10, "Tab")]
            )

    def rot_cw(self, macropad, key, pressed):
        if self.group.selected == 10:
            Action().key(pressed, [Keycode.CONTROL, Keycode.PAGE_DOWN])
        else:
            page_down(macropad, key, pressed)

    def rot_ccw(self, macropad, key, pressed):
        if self.group.selected == 10:
            Action().key(pressed, [Keycode.CONTROL, Keycode.PAGE_UP])
        else:
            page_up(macropad, key, pressed)

def key(code):
    def action(macropad, key, pressed):
        Action().key(pressed, code)
    return action

def build_application():
    app = App()

    configuration = skeleton.Configuration("Firefox")
    configuration.registerKey(0,  "^r",   reload,     0x000500)
    configuration.registerKey(1,  "^t",   tab,        0x000005)
    configuration.registerKey(2,  "x",    close,      0x050000)
    configuration.registerKey(3,  "<",    key([Keycode.CONTROL, Keycode.PAGE_UP]), 0x050505)
    configuration.registerKey(5,  ">",    key([Keycode.CONTROL, Keycode.PAGE_DOWN]),   0x050505)
    configuration.registerKey(12, "",     app.rot_cw, None)
    configuration.registerKey(13, "",     app.rot_ccw,None)
    #configuration.registerGroup(app.group)
    return configuration

configuration = build_application()
